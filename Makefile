#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apietush <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/07 21:31:27 by apietush          #+#    #+#              #
#    Updated: 2017/10/07 21:31:32 by apietush         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = wolf3d
SRCS = sources
FLAGS = -Wall -Wextra -Werror -o
SRC = $(SRCS)/main.c \
		$(SRCS)/presets.c \
		$(SRCS)/predda.c \
		$(SRCS)/draw.c \
		$(SRCS)/hooks.c \
	 
OBJ = $(SRC:.c=.o)
LIBFT = ./libft

all: $(NAME)

$(NAME): $(OBJ)
		@make -C $(LIBFT)
		@gcc $(FLAGS) $(NAME) $(OBJ) $(LIBFT)/libft.a -lmlx -framework OpenGL -framework AppKit

%.o: %.c
		@gcc -c $(FLAGS) $@ $<

clean:
		@rm -f $(OBJ)
		@make clean -C $(LIBFT)

fclean: clean
		@rm -f $(NAME)
		@rm -f $(OBJ)
		@make fclean -C $(LIBFT)

re:		fclean all
