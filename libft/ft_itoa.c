/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 17:07:11 by apietush          #+#    #+#             */
/*   Updated: 2016/12/11 13:39:45 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_countdigits(int n)
{
	size_t	numbers;

	numbers = 0;
	if (n <= 0)
		numbers++;
	while (n != 0)
	{
		n = n / 10;
		numbers++;
	}
	return (numbers);
}

char		*ft_itoa(int n)
{
	size_t	len;
	char	*string;
	size_t	i;

	i = 0;
	len = ft_countdigits(n);
	if ((string = ft_strnew(len)) == NULL)
		return (NULL);
	if (n < 0)
		string[0] = '-';
	if (n == 0)
		string[0] = '0';
	while (n != 0)
	{
		if (n > 0)
			string[len - 1] = '0' + (n % 10);
		if (n < 0)
			string[len - 1] = '0' - (n % 10);
		n = n / 10;
		len--;
	}
	return (string);
}
