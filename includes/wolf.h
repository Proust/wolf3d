/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/14 16:07:33 by apietush          #+#    #+#             */
/*   Updated: 2017/10/14 16:07:34 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H
# define MAP_W 24
# define MAP_H 24
# define WIN_WIDTH 1200
# define WIN_HEIGHT 1000
# include "mlx.h"
# include <math.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <fcntl.h>
# include <stdio.h>
# include <time.h>
# include "../libft/libft.h"

typedef struct		s_textures
{
	char			*brick;
	char			*mossy;
	int				w_brick;
	int				h_brick;
	int				w_mossy;
	int				h_mossy;
	int				tex_y;
	int				tex_x;
}					t_textures;

typedef struct		s_mlx
{
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img;
	char			*arr;
	int				bits_per_pixel;
	int				size_line;
	int				endian;
}					t_mlx;

typedef struct		s_pos
{
	double			pos_x;
	double			pos_y;
	double			dir_x;
	double			dir_y;
	double			olddir_x;
	double			plane_x;
	double			plane_y;
	double			oldplane_x;
	int				pxcol;
	int				h;
}					t_pos;

typedef struct		s_rays
{
	double			time;
	double			old_time;
	double			camera_x;
	double			raypos_x;
	double			raypos_y;
	double			raydir_x;
	double			raydir_y;
}					t_rays;

typedef struct		s_floor
{
	double			floor_xwall;
	double			floor_ywall;
	double			dist_wall;
	double			dist_player;
	double			current_dist;
	int				floortex_x;
	int				floortex_y;
	double			currentfloor_x;
	double			currentfloor_y;
	double			weight;
}					t_floor;

typedef struct		s_dda
{
	int				map_x;
	int				map_y;
	double			sidedist_x;
	double			sidedist_y;
	double			deltadist_x;
	double			deltadist_y;
	double			perpwalldist;
	int				step_x;
	int				step_y;
	int				hit;
	int				side;
	int				lineheight;
	int				drawstart;
	int				drawend;
}					t_dda;

typedef struct		s_wolf
{
	t_pos			pos;
	t_rays			rays;
	t_dda			dda;
	t_mlx			mlx;
	t_textures		tex;
	t_floor			floor;
	int				c;
	int				x;
	int				y;
	int				d;
	int				b;
	int				s;
	int				e;
	double			wall_x;
	double			*tex_buff;
	double			frametime;
	double			movespeed;
	double			rotspeed;
	int				worldmap[MAP_W][MAP_H];
}					t_wolf;

void				up_down(int keycode, t_wolf *wolf);
void				left_right(int keycode, t_wolf *wolf);
int					exit_x(void);
void				load_textures(t_wolf *wolf);
void				set_params(t_wolf *wolf);
void				basic_init(t_wolf *wolf);
void				walker_two(t_wolf *wolf);
void				step_calc(t_wolf *wolf);
void				draw_calc(t_wolf *wolf);
void				walls_calc(t_wolf *wolf);
void				floor_ceiling_calc(t_wolf *wolf);
void				walls_draw(t_wolf *wolf);
void				floor_ceiling_draw(t_wolf *wolf);
int					key_hook(int keycode, t_wolf *wolf);
void				rendering(t_wolf *wolf);

#endif
