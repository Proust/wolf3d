/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   presets.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/21 14:02:08 by apietush          #+#    #+#             */
/*   Updated: 2017/10/21 14:02:10 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf.h"

void	basic_init(t_wolf *wolf)
{
	wolf->rays.camera_x = 2 * wolf->x / (double)(WIN_WIDTH) - 1;
	wolf->rays.raypos_x = wolf->pos.pos_x;
	wolf->rays.raypos_y = wolf->pos.pos_y;
	wolf->rays.raydir_x = wolf->pos.dir_x + wolf->pos.plane_x *
											wolf->rays.camera_x;
	wolf->rays.raydir_y = wolf->pos.dir_y + wolf->pos.plane_y *
											wolf->rays.camera_x;
	wolf->dda.map_x = (int)(wolf->rays.raypos_x);
	wolf->dda.map_y = (int)(wolf->rays.raypos_y);
	wolf->dda.deltadist_x = sqrt(1 + (wolf->rays.raydir_y * wolf->rays.raydir_y)
								/ (wolf->rays.raydir_x * wolf->rays.raydir_x));
	wolf->dda.deltadist_y = sqrt(1 + (wolf->rays.raydir_x * wolf->rays.raydir_x)
								/ (wolf->rays.raydir_y * wolf->rays.raydir_y));
	wolf->dda.hit = 0;
}

void	set_params(t_wolf *wolf)
{
	wolf->pos.pos_x = 12;
	wolf->pos.pos_y = 22;
	wolf->pos.dir_x = -1.0;
	wolf->pos.dir_y = 0.0;
	wolf->pos.plane_x = 0.0;
	wolf->pos.plane_y = 0.66;
	wolf->rays.time = 0.0;
	wolf->rays.old_time = 0.0;
	wolf->movespeed = 0.3;
	wolf->rotspeed = 0.3;
}

void	load_textures(t_wolf *wolf)
{
	void	*img[2];

	img[0] = mlx_xpm_file_to_image(wolf->mlx.mlx_ptr,
	"xpm/my/gray_floor.xpm", &wolf->tex.w_brick, &wolf->tex.h_brick);
	img[1] = mlx_xpm_file_to_image(wolf->mlx.mlx_ptr,
	"xpm/my/pixel_wall.xpm", &wolf->tex.w_mossy, &wolf->tex.h_mossy);
	wolf->tex.brick = mlx_get_data_addr(img[0], &wolf->b, &wolf->s, &wolf->e);
	wolf->tex.mossy = mlx_get_data_addr(img[1], &wolf->b, &wolf->s, &wolf->e);
}
