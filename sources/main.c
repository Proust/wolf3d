/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/14 15:49:24 by apietush          #+#    #+#             */
/*   Updated: 2017/10/14 15:49:26 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf.h"

void	map_generator(t_wolf *wolf)
{
	int		x;
	int		y;

	x = 0;
	srand(time(NULL));
	while (x < MAP_W)
	{
		y = 0;
		while (y < MAP_H)
		{
			if (((x == 0 || x == MAP_W - 1 || y == 0 || y == MAP_H - 1)
					|| rand() % 100 > 90) && !((x == 12 && y == 22))
				&& !((x == 11 && y == 22)) && !((x == 13 && y == 22))
				&& !((x == 12 && y == 23)) && !((x == 12 && y == 21))
				&& !((x == 11 && y == 21)) && !((x == 13 && y == 23))
				&& !((x == 11 && y == 23)) && !((x == 13 && y == 21)))
				wolf->worldmap[x][y] = 1;
			else
				wolf->worldmap[x][y] = 0;
			y++;
		}
		x++;
	}
}

void	calculating(t_wolf *wolf)
{
	wolf->x = 0;
	load_textures(wolf);
	while (wolf->x < WIN_WIDTH)
	{
		basic_init(wolf);
		step_calc(wolf);
		while (wolf->dda.hit == 0)
			walker_two(wolf);
		draw_calc(wolf);
		walls_calc(wolf);
		while (wolf->y < wolf->dda.drawend)
			walls_draw(wolf);
		floor_ceiling_calc(wolf);
		while (wolf->y < WIN_HEIGHT)
		{
			floor_ceiling_draw(wolf);
			wolf->y++;
		}
		wolf->x++;
	}
}

void	rendering(t_wolf *wolf)
{
	wolf->mlx.img = mlx_new_image(wolf->mlx.mlx_ptr, WIN_WIDTH, WIN_HEIGHT);
	wolf->mlx.arr = mlx_get_data_addr(wolf->mlx.img, &wolf->mlx.bits_per_pixel,
									&wolf->mlx.size_line, &wolf->mlx.endian);
	calculating(wolf);
	mlx_put_image_to_window(wolf->mlx.mlx_ptr, wolf->mlx.win_ptr,
											wolf->mlx.img, 0, 0);
	mlx_destroy_image(wolf->mlx.mlx_ptr, wolf->mlx.img);
}

int		main(int argc, char **argv)
{
	t_wolf	wolf;

	if (argc != 1 && argv)
	{
		ft_putstr("Maps are randomly generated from game to game. Have fun!\n");
		exit(0);
	}
	map_generator(&wolf);
	wolf.mlx.mlx_ptr = mlx_init();
	wolf.mlx.win_ptr = mlx_new_window(wolf.mlx.mlx_ptr,
						WIN_WIDTH, WIN_HEIGHT, "Wolf3D");
	set_params(&wolf);
	rendering(&wolf);
	mlx_hook(wolf.mlx.win_ptr, 2, 0, key_hook, &wolf);
	mlx_hook(wolf.mlx.win_ptr, 17, 0, exit_x, &wolf);
	system("afplay music.mp3 &");
	mlx_loop(wolf.mlx.mlx_ptr);
	return (0);
}
