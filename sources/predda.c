/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   predda.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/21 14:01:47 by apietush          #+#    #+#             */
/*   Updated: 2017/10/21 14:01:49 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf.h"

void	walker_two(t_wolf *wolf)
{
	if (wolf->dda.sidedist_x < wolf->dda.sidedist_y)
	{
		wolf->dda.sidedist_x += wolf->dda.deltadist_x;
		wolf->dda.map_x += wolf->dda.step_x;
		wolf->dda.side = 0;
	}
	else
	{
		wolf->dda.sidedist_y += wolf->dda.deltadist_y;
		wolf->dda.map_y += wolf->dda.step_y;
		wolf->dda.side = 1;
	}
	if (wolf->worldmap[wolf->dda.map_x][wolf->dda.map_y] > 0)
		wolf->dda.hit = 1;
}

void	step_calc(t_wolf *wolf)
{
	if (wolf->rays.raydir_x < 0)
	{
		wolf->dda.step_x = -1;
		wolf->dda.sidedist_x = (wolf->rays.raypos_x - wolf->dda.map_x)
											* wolf->dda.deltadist_x;
	}
	else
	{
		wolf->dda.step_x = 1;
		wolf->dda.sidedist_x = (wolf->dda.map_x + 1.0 - wolf->rays.raypos_x)
											* wolf->dda.deltadist_x;
	}
	if (wolf->rays.raydir_y < 0)
	{
		wolf->dda.step_y = -1;
		wolf->dda.sidedist_y = (wolf->rays.raypos_y - wolf->dda.map_y)
											* wolf->dda.deltadist_y;
	}
	else
	{
		wolf->dda.step_y = 1;
		wolf->dda.sidedist_y = (wolf->dda.map_y + 1.0 - wolf->rays.raypos_y)
											* wolf->dda.deltadist_y;
	}
}

void	draw_calc(t_wolf *wolf)
{
	if (wolf->dda.side == 0)
		wolf->dda.perpwalldist = (wolf->dda.map_x - wolf->rays.raypos_x
					+ (1 - wolf->dda.step_x) / 2) / wolf->rays.raydir_x;
	else
		wolf->dda.perpwalldist = (wolf->dda.map_y - wolf->rays.raypos_y
					+ (1 - wolf->dda.step_y) / 2) / wolf->rays.raydir_y;
	wolf->dda.lineheight = (int)(WIN_HEIGHT / wolf->dda.perpwalldist);
	wolf->dda.drawstart = -wolf->dda.lineheight / 2 + WIN_HEIGHT / 2;
	if (wolf->dda.drawstart < 0)
		wolf->dda.drawstart = 0;
	wolf->dda.drawend = wolf->dda.lineheight / 2 + WIN_HEIGHT / 2;
	if (wolf->dda.drawend >= WIN_HEIGHT)
		wolf->dda.drawend = WIN_HEIGHT - 1;
}

void	walls_calc(t_wolf *wolf)
{
	if (wolf->dda.side == 0)
		wolf->wall_x = wolf->rays.raypos_y + wolf->dda.perpwalldist
											* wolf->rays.raydir_y;
	else
		wolf->wall_x = wolf->rays.raypos_x + wolf->dda.perpwalldist
											* wolf->rays.raydir_x;
	wolf->wall_x -= floor((wolf->wall_x));
	wolf->tex.tex_x = (int)(wolf->wall_x * (double)(wolf->tex.w_brick));
	if (wolf->dda.side == 0 && wolf->rays.raydir_x > 0)
		wolf->tex.tex_x = wolf->tex.w_brick - wolf->tex.tex_x - 1;
	if (wolf->dda.side == 1 && wolf->rays.raydir_y < 0)
		wolf->tex.tex_x = wolf->tex.w_brick - wolf->tex.tex_x - 1;
	wolf->y = wolf->dda.drawstart;
}

void	floor_ceiling_calc(t_wolf *wolf)
{
	if (wolf->dda.side == 0 && wolf->rays.raydir_x > 0)
	{
		wolf->floor.floor_xwall = wolf->dda.map_x;
		wolf->floor.floor_ywall = wolf->dda.map_y + wolf->wall_x;
	}
	else if (wolf->dda.side == 0 && wolf->rays.raydir_x < 0)
	{
		wolf->floor.floor_xwall = wolf->dda.map_x + 1.0;
		wolf->floor.floor_ywall = wolf->dda.map_y + wolf->wall_x;
	}
	else if (wolf->dda.side == 1 && wolf->rays.raydir_y > 0)
	{
		wolf->floor.floor_xwall = wolf->dda.map_x + wolf->wall_x;
		wolf->floor.floor_ywall = wolf->dda.map_y;
	}
	else
	{
		wolf->floor.floor_xwall = wolf->dda.map_x + wolf->wall_x;
		wolf->floor.floor_ywall = wolf->dda.map_y + 1.0;
	}
	wolf->floor.dist_wall = wolf->dda.perpwalldist;
	wolf->floor.dist_player = 0.0;
	if (wolf->dda.drawend < 0)
		wolf->dda.drawend = WIN_HEIGHT;
	wolf->y = wolf->dda.drawend + 1;
}
