/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/21 14:01:20 by apietush          #+#    #+#             */
/*   Updated: 2017/10/21 14:01:22 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf.h"

void	walls_draw(t_wolf *wolf)
{
	wolf->d = wolf->y * 256 - WIN_HEIGHT * 128 + wolf->dda.lineheight * 128;
	wolf->tex.tex_y = ((wolf->d * wolf->tex.h_brick) / wolf->dda.lineheight)
																		/ 256;
	wolf->c = ((wolf->tex.w_brick * wolf->tex.tex_y) + wolf->tex.tex_x) * 4;
	wolf->pos.pxcol = (wolf->x * 4) + (wolf->y * ((WIN_WIDTH) * 4));
	wolf->mlx.arr[wolf->pos.pxcol] = wolf->tex.brick[wolf->c];
	wolf->mlx.arr[wolf->pos.pxcol + 1] = wolf->tex.brick[wolf->c + 1];
	wolf->mlx.arr[wolf->pos.pxcol + 2] = wolf->tex.brick[wolf->c + 2];
	wolf->y++;
}

void	floor_ceiling_draw(t_wolf *wolf)
{
	wolf->floor.current_dist = WIN_HEIGHT / (2.0 * wolf->y - WIN_HEIGHT);
	wolf->floor.weight = (wolf->floor.current_dist - wolf->floor.dist_player)
						/ (wolf->floor.dist_wall - wolf->floor.dist_player);
	wolf->floor.currentfloor_x = wolf->floor.weight * wolf->floor.floor_xwall
						+ (1.0 - wolf->floor.weight) * wolf->pos.pos_x;
	wolf->floor.currentfloor_y = wolf->floor.weight * wolf->floor.floor_ywall
						+ (1.0 - wolf->floor.weight) * wolf->pos.pos_y;
	wolf->floor.floortex_x = (int)(wolf->floor.currentfloor_x
						* wolf->tex.w_mossy) % wolf->tex.w_mossy;
	wolf->floor.floortex_y = (int)(wolf->floor.currentfloor_y
						* wolf->tex.h_mossy) % wolf->tex.h_mossy;
	wolf->c = ((wolf->tex.w_mossy * wolf->floor.floortex_y)
						+ wolf->floor.floortex_x) * 4;
	wolf->pos.pxcol = (wolf->x * 4) + (wolf->y * ((WIN_WIDTH) * 4));
	wolf->mlx.arr[wolf->pos.pxcol] = wolf->tex.mossy[wolf->c];
	wolf->mlx.arr[wolf->pos.pxcol + 1] = wolf->tex.mossy[wolf->c + 1];
	wolf->mlx.arr[wolf->pos.pxcol + 2] = wolf->tex.mossy[wolf->c + 2];
	wolf->pos.pxcol = (wolf->x * 4) + ((WIN_HEIGHT - wolf->y)
						* (WIN_WIDTH * 4));
	wolf->mlx.arr[wolf->pos.pxcol] = wolf->tex.mossy[wolf->c];
	wolf->mlx.arr[wolf->pos.pxcol + 1] = wolf->tex.mossy[wolf->c + 1];
	wolf->mlx.arr[wolf->pos.pxcol + 2] = wolf->tex.mossy[wolf->c + 2];
}
