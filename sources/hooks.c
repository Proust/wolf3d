/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/21 14:01:33 by apietush          #+#    #+#             */
/*   Updated: 2017/10/21 14:01:34 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf.h"

void	up_down(int keycode, t_wolf *wolf)
{
	if (keycode == 126)
	{
		if (wolf->worldmap[(int)(wolf->pos.pos_x + ((wolf->pos.dir_x
			* wolf->movespeed) * 3))][(int)wolf->pos.pos_y] == 0)
			wolf->pos.pos_x += wolf->pos.dir_x * wolf->movespeed;
		if (wolf->worldmap[(int)wolf->pos.pos_x]
			[(int)(wolf->pos.pos_y + ((wolf->pos.dir_y
										* wolf->movespeed) * 3))] == 0)
			wolf->pos.pos_y += wolf->pos.dir_y * wolf->movespeed;
	}
	if (keycode == 125)
	{
		if (wolf->worldmap[(int)(wolf->pos.pos_x - ((wolf->pos.dir_x
						* wolf->movespeed) * 3))][(int)(wolf->pos.pos_y)] == 0)
			wolf->pos.pos_x -= wolf->pos.dir_x * wolf->movespeed;
		if (wolf->worldmap[(int)(wolf->pos.pos_x)]
			[(int)(wolf->pos.pos_y - ((wolf->pos.dir_y * wolf->movespeed)
			* 3))] == 0)
			wolf->pos.pos_y -= wolf->pos.dir_y * wolf->movespeed;
	}
	if (keycode == 53)
	{
		system("killall afplay");
		exit(0);
	}
}

void	left(t_wolf *wolf)
{
	wolf->pos.olddir_x = wolf->pos.dir_x;
	wolf->pos.dir_x = wolf->pos.dir_x * cos(wolf->rotspeed)
					- wolf->pos.dir_y * sin(wolf->rotspeed);
	wolf->pos.dir_y = wolf->pos.olddir_x * sin(wolf->rotspeed)
					+ wolf->pos.dir_y * cos(wolf->rotspeed);
	wolf->pos.oldplane_x = wolf->pos.plane_x;
	wolf->pos.plane_x = wolf->pos.plane_x * cos(wolf->rotspeed)
					- wolf->pos.plane_y * sin(wolf->rotspeed);
	wolf->pos.plane_y = wolf->pos.oldplane_x * sin(wolf->rotspeed)
					+ wolf->pos.plane_y * cos(wolf->rotspeed);
}

void	left_right(int keycode, t_wolf *wolf)
{
	if (keycode == 124)
	{
		wolf->pos.olddir_x = wolf->pos.dir_x;
		wolf->pos.dir_x = wolf->pos.dir_x * cos(-wolf->rotspeed)
						- wolf->pos.dir_y * sin(-wolf->rotspeed);
		wolf->pos.dir_y = wolf->pos.olddir_x * sin(-wolf->rotspeed)
						+ wolf->pos.dir_y * cos(-wolf->rotspeed);
		wolf->pos.oldplane_x = wolf->pos.plane_x;
		wolf->pos.plane_x = wolf->pos.plane_x * cos(-wolf->rotspeed)
						- wolf->pos.plane_y * sin(-wolf->rotspeed);
		wolf->pos.plane_y = wolf->pos.oldplane_x * sin(-wolf->rotspeed)
						+ wolf->pos.plane_y * cos(-wolf->rotspeed);
	}
	if (keycode == 123)
		left(wolf);
}

int		exit_x(void)
{
	system("killall afplay");
	exit(0);
}

int		key_hook(int keycode, t_wolf *wolf)
{
	up_down(keycode, wolf);
	left_right(keycode, wolf);
	rendering(wolf);
	return (0);
}
